var kue = require('kue')
, redis = require('redis');

  kue.redis.createClient = function() {
    var client = redis.createClient(6379, '127.0.0.1');
    client.auth('');
    return client;
  };
  var jobs = kue.createQueue();
	
var sequence = 0;

setInterval(
  function() {
  sequence += 1;
  (function(sequence) {
  var job = jobs.create('email',{
  title: 'Hello #' + sequence
  ,to: 'youremailhere@gmail.com'
  ,body: 'Hello from node!'
  }).save();
  
  job.on('complete', function(){
  console.log('job ' + sequence + ' completed!')
  });
  
  job.on('failed', function() {
  console.log('job ' + sequence + 'failed!')
  });
  })(sequence);
  }
  ,1000);
	
	